<?php

function selectLocation(){
  include('../model/connectdb.php');
  $sql = "SELECT LocationID, Name FROM [Production].[Location]";
  $stmt = sqlsrv_query( $conn, $sql );
  if( $stmt === false) {
    die( print_r( sqlsrv_errors(), true) );
  }
  echo "<option value='all'>Select Lacation</option>";
  while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
    echo "<option value='".$row['LocationID']."'>".$row['Name']."</option>";
  }

  sqlsrv_free_stmt( $stmt);
}


function selectProduct(){
  include('../model/connectdb.php');
  $sql = "SELECT ProductID, Name FROM [Production].[Product]";
  $stmt = sqlsrv_query( $conn, $sql );
  if( $stmt === false) {
    die( print_r( sqlsrv_errors(), true) );
  }
  echo "<option value='all'>Select Product</option>";
  while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
    echo "<option value='".$row['ProductID']."'>".$row['Name']."</option>";
  }

  sqlsrv_free_stmt( $stmt);
}


function showRecord($tableName){

  if($tableName!="all"){
    include('../../model/connectdb.php');
    $sql = "SELECT PI.ProductID, PI.LocationID, PI.Shelf, PI.Bin, PI.Quantity , P.Name AS Pname, CONVERT([varchar](MAX),PP.ThumbNailPhoto,2) AS TNphoto
    FROM Production.ProductInventory PI
    JOIN Production.Product AS P
    ON PI.ProductID = P.ProductID
    JOIN Production.ProductProductPhoto AS PPP
    ON PI.ProductID = PPP.ProductID
    JOIN Production.ProductPhoto AS PP
    ON PPP.ProductPhotoID = PP.ProductPhotoID
    WHERE PI.LocationID = ".$tableName."
    ORDER BY PI.ProductID , PI.LocationID";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
      die( print_r( sqlsrv_errors(), true) );
    }
    echo "<div class='col-md-7'><div class='scrollbar'>";
    //loop print
    $i = 1;
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      $d=pack('H*',strtolower($row['TNphoto']));
      echo "<div style='width: 610px; margin-bottom: 15px; padding-top: 20px; padding-bottom: 20px;background-color: white; box-shadow:  1px 1px 1px 1px #B5B5B5;'>
      <div class='row'>
        <div class='col-md-4' id='images'>
          <img src='../pages/Components/imageSrc.php?imageProduct=".$row['TNphoto']."' style='width:80%;'></>
        </div>
        <div class='col-md-2'>
          <b>ProductID:</b>
          <br /><b>Name:</b>
          <br /><b>Shelf:</b>
          <br /><b>Bin:</b>
          <br /><b>Quantity:</b>
        </div>
        <div class='col-md-6'>
          <div id='testnaja'>".$row['ProductID']."</div>"
          .$row['Pname'].
          "<br />".$row['Shelf'].
          "<br />".$row['Bin'].
          "<br />".$row['Quantity'].
          "</div>
        </div>
        <div class='row'>
          <div class='col-md-9'>
          </div>
          <div class='col-md-1'>
            <div class='icon-bar'>
              <button class='btn btn-default' onClick='showEditto(\"".$row['Shelf']."\",".$row['Bin'].",".$row['Quantity'].",".$row['ProductID'].",".$row['LocationID'].")'>Edit</button>
            </div>
          </div>
          <div class='col-md-1'>
          <button class='btn btn-default' onClick=\"delete_by_id(".$row['ProductID'].",".$row['LocationID'].")\">Delete</button>
          </div>
          <div class='col-md-1'>
          </div>
        </div>
      </div>";
    }
    echo "</div></div>";
    echo '<div class="col-md-5">';
    echo  '
    <form action="" method="post" name="formEditSubmit" id="formEditSubmit">
      <div class="formEdit">
        <div class="row form-group">
          <div class="col-sm-10">
            <label for="pidS">ProductID:</label>
            <input type="text" class="inputProductEdit" id="pidS" name="pidS" disabled>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-10">
            <label for="shelfE">Shelf:</label>
            <input type="text" class="form-control" id="shelfE" name="shelfE" disabled>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-10">
            <label for="bin">Bin:</label>
            <input type="text" class="form-control" id="binE" name="binE" disabled>
          </div>
        </div>
        <div class="row form-group">
          <div class="col-sm-10">
            <label for="quantity">Quantity:</label>
            <input type="number" class="form-control" id="quantityE" name="quantityE" disabled>
          </div>
        </div>
        <br>
        <input type="hidden" name="pidE" id="pidE" value="">
        <input type="hidden" name="lidE" id="lidE" value="">
        <button type="submit" name="updatebutton"  id="updatebutton" class="btn btn-default" disabled>Update</button>

      </div>
    </form>';
    echo '</div>';

    sqlsrv_free_stmt( $stmt);
  }

  if(isset($_POST['shelfE'])&&isset($_POST['binE'])&&isset($_POST['quantityE'])&&isset($_POST['pidE'])&&isset($_POST['lidE'])){

    $productID = $_POST['pidE'];
    $shelf = $_POST['shelfE'];
    $bin = $_POST['binE'];
    $quantity = $_POST['quantityE'];
    $locationID = $_POST['lidE'];

    include('../model/connectdb.php');
    $sql = "UPDATE [Production].[ProductInventory]
    SET Shelf = '".$shelf."', Bin = '".$bin."', Quantity = '".$quantity."'
    WHERE ProductID = '".$productID."' AND LocationID = ".$locationID;
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
      die( print_r( sqlsrv_errors(), true) );
    }
    sqlsrv_free_stmt( $stmt);

    echo "<script>afterEditContent(".$_POST['lidE'].");</script>";
  }

}



?>
