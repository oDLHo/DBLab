<?php

  function getSaleRepresent(){
    include('./../model/connectdb.php');
      $sql = "SELECT P.BusinessEntityID,HM.Gender AS Gender,P.FirstName AS FirstName,P.LastName AS LastName,TS.total AS Total
        FROM HumanResources.Employee AS HM
        JOIN(
          SELECT TOP 5 SO.SalesPersonID,count(SO.SalesPersonID) AS total
          FROM Sales.SalesOrderHeader SO INNER JOIN Sales.SalesPerson SP
          ON  SO.SalesPersonID=SP.BusinessEntityID
          Group by SO.SalesPersonID
          Order by total DESC
          ) TS
        ON HM.BusinessEntityID = TS.SalesPersonID
        JOIN Person.Person P
        ON P.BusinessEntityID=HM.BusinessEntityID";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    $i=1;
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      if($row['Gender']="F"){ $gender="MS.";}
      else { $gender="MR.";}
      echo '<li class="list-group-item">'.$i++.". ".$gender.' '.$row['FirstName']." ".$row['LastName'].'<span class="badge">'.$row['Total'].'</span></li>';
    }
    echo "<br>";
    sqlsrv_free_stmt( $stmt);
  }

  function getNumberOfEmployee(){
    include('./../model/connectdb.php');
      $sql = "SELECT count(*) AS Num
        FROM
        (
        SELECT EM.BusinessEntityID AS id1 From HumanResources.Employee EM
        EXCEPT
        SELECT  EMP.BusinessEntityID AS id2 FROM HumanResources.Employee EMP where JobTitle='Chief Executive Officer'
        ) a;";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    $i=1;
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      echo "<h3>Number of Employee </h3><br><h2>".$row['Num']." PERSONS </h2>";
    }

    sqlsrv_free_stmt( $stmt);
  }

  function getOutOfStock(){
    include('./../model/connectdb.php');
      $sql = "SELECT Name
        FROM Production.Product
        WHERE ProductID
        IN(
        SELECT ProductID FROM Production.Product WHERE SellEndDate IS NULL
        EXCEPT
        SELECT ProductID FROM Production.ProductInventory)";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    $i=1;
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      echo '<li class="list-group-item">'.$i++.". ".$row['Name'].'</li>';
    }

    sqlsrv_free_stmt( $stmt);
  }

  function getNumberOfOutOfStock(){
    include('./../model/connectdb.php');
      $sql = "SELECT COUNT(*) AS Num
        FROM Production.Product
        WHERE ProductID
        IN(
        SELECT ProductID FROM Production.Product WHERE SellEndDate IS NULL
        EXCEPT
        SELECT ProductID FROM Production.ProductInventory)
        ";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    $i=1;
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      echo "<h3>Out of stock products </h3><br><h2>".$row['Num']." products </h2>";
    }

    sqlsrv_free_stmt( $stmt);
  }

  function getTotalSale(){
    include('./../model/connectdb.php');
      $sql = "SELECT year(OrderDate) AS Year,SUM(TotalDue) AS [TotalSales] FROM [AdventureWorks2014].[Sales].[SalesOrderHeader]
        Group by year(OrderDate) Order by year(OrderDate)";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      echo "<tr>";
      echo "<td>".$row['Year']."</td><td> $ ".$row['TotalSales']."</td>";
      echo "</tr>";
    }
    sqlsrv_free_stmt( $stmt);
  }

  function getCardType(){
    include('./../model/connectdb.php');
      $sql = "SELECT CardType,count(CardType) * 100.0 / sum(count(CardType)) over()  AS Percentage,count(CardType) AS NumOfUse FROM Sales.SalesOrderHeader SO INNER JOIN Sales.CreditCard CR ON SO.CreditCardID=CR.CreditCardID Group BY CardType";
    $stmt = sqlsrv_query( $conn, $sql );
    if( $stmt === false) {
        die( print_r( sqlsrv_errors(), true) );
    }
    while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      echo "<tr>";
      echo "<td>".$row['CardType']."</td><td>".$row['NumOfUse']."</td>";
      echo "</tr>";
    }
    sqlsrv_free_stmt( $stmt);
  }

?>
