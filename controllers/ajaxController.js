function showContentRecord() {
  var table = document.getElementById('tableContentSelecter').value;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      document.getElementById("recordHandle").innerHTML = xmlhttp.responseText;
    }
  };
  xmlhttp.open("GET", "./../pages/Components/ajaxContent.php?tableName="+table, true);
  xmlhttp.send();
}

function showForm() {
  var table = document.getElementById('tableFormSelecter').value;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      document.getElementById("insertFormHandle").innerHTML = xmlhttp.responseText;
    }
  };
  xmlhttp.open("GET", "./../pages/Components/InsertForm.php?tableName="+table, true);
  xmlhttp.send();
}

function showReport(reportNum){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      document.getElementById("reportHandle").innerHTML = xmlhttp.responseText;
    }
  };
  xmlhttp.open("GET", "./../pages/Components/ajaxReport.php?reportNum="+reportNum, true);
  xmlhttp.send();
}



function delete_by_id(pid,lid) {
  var pidd = pid;
  var lidd = lid;
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      alert("Delete completed");
      document.getElementById("recordHandle").innerHTML = xmlhttp.responseText;
    }

  };
  xmlhttp.open("GET", "./../pages/Components/ajaxdelete.php?pidd="+pidd+"&lidd="+lidd, true);
  xmlhttp.send();
}

