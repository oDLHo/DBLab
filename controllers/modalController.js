function showEditto(shf,bin,qua,pid,lid){
  document.getElementById("shelfE").disabled = false;
  document.getElementById("binE").disabled = false;
  document.getElementById("quantityE").disabled = false;
  document.getElementById("pidE").disabled = false;
  document.getElementById("lidE").disabled = false;
  document.getElementById("updatebutton").disabled = false;

  document.getElementById("shelfE").value = shf;
  document.getElementById("binE").value = bin;
  document.getElementById("quantityE").value = qua;
  document.getElementById("pidE").value = pid;
  document.getElementById("lidE").value = lid;
  document.getElementById("pidS").value = pid;
}

function showInsert(){
  // animate the step out
  var $contentModal = $('#contentHandle');
  var $insertModal = $('#insertHandle');


  $contentModal.addClass('animate-out');
  // animate the step in
  setTimeout(function(){
    $contentModal.removeClass('animate-in animate-out isShowing');
    $insertModal.addClass('animate-in isShowing');
  }, 1000);
  // after the animation, adjust the classes
  setTimeout(function(){
    $insertModal.removeClass('animate-in')
  }, 1500);
}

function showContent(){
  console.log('showContent');
  // animate the step out
  var $contentModal = $('#contentHandle');
  var $insertModal = $('#insertHandle');

  $insertModal.addClass('animate-out');
  // animate the step in
  setTimeout(function(){
    $insertModal.removeClass('animate-in animate-out isShowing');
    $contentModal.addClass('animate-in isShowing');
  }, 1000);
  // after the animation, adjust the classes
  setTimeout(function(){
    $contentModal.removeClass('animate-in')
  }, 1500);
}

function showSaleReport(){
  // animate the step out
  var $productModal = $('#productHandle');
  var $saleModal = $('#saleHandle');
  var $hrModal = $('#hrHandle');

  $productModal.addClass('animate-out');
  $hrModal.addClass('animate-out');
  // animate the step in
  setTimeout(function(){
    $productModal.removeClass('animate-in animate-out isShowing');
    $hrModal.removeClass('animate-in animate-out isShowing');
    $saleModal.addClass('animate-in isShowing');
  }, 1000);
  // after the animation, adjust the classes
  setTimeout(function(){
    $saleModal.removeClass('animate-in')
  }, 1500);
}

function showHRReport(){
  // animate the step out
  var $productModal = $('#productHandle');
  var $saleModal = $('#saleHandle');
  var $hrModal = $('#hrHandle');

  $productModal.addClass('animate-out');
  $saleModal.addClass('animate-out');
  // animate the step in
  setTimeout(function(){
    $productModal.removeClass('animate-in animate-out isShowing');
    $saleModal.removeClass('animate-in animate-out isShowing');
    $hrModal.addClass('animate-in isShowing');
  }, 1000);
  // after the animation, adjust the classes
  setTimeout(function(){
    $hrModal.removeClass('animate-in')
  }, 1500);
}

function showProductReport(){
  // animate the step out
  var $productModal = $('#productHandle');
  var $saleModal = $('#saleHandle');
  var $hrModal = $('#hrHandle');

  $saleModal.addClass('animate-out');
  $hrModal.addClass('animate-out');
  // animate the step in
  setTimeout(function(){
    $saleModal.removeClass('animate-in animate-out isShowing');
    $hrModal.removeClass('animate-in animate-out isShowing');
    $productModal.addClass('animate-in isShowing');
  }, 1000);
  // after the animation, adjust the classes
  setTimeout(function(){
    $productModal.removeClass('animate-in')
  }, 1500);
}

function afterEditContent(lid){

  var $contentModal = $('#contentHandle');
  var $insertModal = $('#insertHandle');
  var lidd = lid

  $insertModal.removeClass('animate-in animate-out isShowing');
  $contentModal.addClass('isShowing');

  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {

    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
      document.getElementById("recordHandle").innerHTML = xmlhttp.responseText;
      alert('Edit Completed');
    }
  };
  xmlhttp.open("GET", "./../pages/Components/ajaxContent.php?tableName="+lidd, true);
  xmlhttp.send();
}
