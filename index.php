<!DOCTYPE html>
<html>
  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Bootstrap style link -->
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="./assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- Custom styles link -->
    <link rel="stylesheet" href="./assets/css/index.css">
    <script type="text/javascript" src="./../assets/js/jquery.js"></script>
    <title>Adventure Work Company</title>
  </head>
  <body>
    <div class="cover-bg">
      <h1 class="title">Adventure Work Company</h1>
      <a class="ghost-button" href="./pages/admin.php">Log in as Admin</a>
      <a class="ghost-button" href="./pages/ceo.php">Log in as CEO</a>
    </div>
  </body>
</html>
