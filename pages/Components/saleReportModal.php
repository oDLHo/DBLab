<div id="graph1Modal" style=" margin-top: 10px;">

	<div class="container-fluid">
		<div class="container">
			<div class="row">
				<h2 class="text-center">SALES</h2>
				<p class="text-center">This is overview of sales</p>
				<div class="col-md-12">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="graph3"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="container-fluid">
		<div class="container">
			<div class="row" style="margin-top: 20px;">
				<div class="col-md-8">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="graph1"></div>
							<br>
							<button class="btn btn-info" id="plain">Plain</button>
							<button class="btn btn-info" id="inverted">Inverted</button>
							<button class="btn btn-info" id="polar">Polar</button>
							<br><br>
					  	<table class="table table-bordered">
						    <thead>
						      <tr>
						        <th>Year</th>
						        <th>Total Sale</th>
						      </tr>
						    </thead>
						    <tbody>
									<?php
										getTotalSale();
									?>
						    </tbody>
							</table>
						</div>
					</div>
				</div>

				<div class="col-md-4">
					<div class="panel panel-default">
						<div class="panel-body">
							<div id="graph2"></div>
							<br>
					 		<table class="table table-bordered">
						    <thead>
						      <tr>
						        <th>Card Type</th>
						        <th>Number Of Using</th>
						      </tr>
						    </thead>
						    <tbody>
									<?php
										getCardType();
										?>
						    </tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
		////////////////////////////////////////////////////////////////////////////////////////////////////
		Highcharts.createElement('link', {
	   	href: 'https://fonts.googleapis.com/css?family=Unica+One',
	   	rel: 'stylesheet',
	   	type: 'text/css'
		}, null, document.getElementsByTagName('head')[0]);

		Highcharts.theme = {
	   	colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066', '#eeaaee',
	      	'#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
	   	chart: {
	      backgroundColor: {
	         linearGradient: { x1: 0, y1: 0, x2: 1, y2: 1 },
	         stops: [
	            [0, '#2a2a2b'],
	            [1, '#3e3e40']
	         ]
	      },
	      style: {
	         fontFamily: 'sans-serif'
	      },
	      plotBorderColor: '#606063'
	   },
	   title: {
	      style: {
	         color: '#E0E0E3',
	         textTransform: 'uppercase',
	         fontSize: '20px'
	      }
	   },
	   subtitle: {
	      style: {
	         color: '#E0E0E3',
	         textTransform: 'uppercase'
	      }
	   },
	   xAxis: {
	      gridLineColor: '#707073',
	      labels: {
	         style: {
	            color: '#E0E0E3'
	         }
	      },
	      lineColor: '#707073',
	      minorGridLineColor: '#505053',
	      tickColor: '#707073',
	      title: {
	         style: {
	            color: '#A0A0A3'

	         }
	      }
	   },
	   yAxis: {
	      gridLineColor: '#707073',
	      labels: {
	         style: {
	            color: '#E0E0E3'
	         }
	      },
	      lineColor: '#707073',
	      minorGridLineColor: '#505053',
	      tickColor: '#707073',
	      tickWidth: 1,
	      title: {
	         style: {
	            color: '#A0A0A3'
	         }
	      }
	   },
	   tooltip: {
	      backgroundColor: 'rgba(0, 0, 0, 0.85)',
	      style: {
	         color: '#F0F0F0'
	      }
	   },
	   plotOptions: {
	      series: {
	         dataLabels: {
	            color: '#B0B0B3'
	         },
	         marker: {
	            lineColor: '#333'
	         }
	      },
	      boxplot: {
	         fillColor: '#505053'
	      },
	      candlestick: {
	         lineColor: 'white'
	      },
	      errorbar: {
	         color: 'white'
	      }
	   },
	   legend: {
	      itemStyle: {
	         color: '#E0E0E3'
	      },
	      itemHoverStyle: {
	         color: '#FFF'
	      },
	      itemHiddenStyle: {
	         color: '#606063'
	      }
	   },
	   credits: {
	      style: {
	         color: '#666'
	      }
	   },
	   labels: {
	      style: {
	         color: '#707073'
	      }
	   },

	   drilldown: {
	      activeAxisLabelStyle: {
	         color: '#F0F0F3'
	      },
	      activeDataLabelStyle: {
	         color: '#F0F0F3'
	      }
	   },

	   navigation: {
	      buttonOptions: {
	         symbolStroke: '#DDDDDD',
	         theme: {
	            fill: '#505053'
	         }
	      }
	   },

	   // scroll charts
	   rangeSelector: {
	      buttonTheme: {
	         fill: '#505053',
	         stroke: '#000000',
	         style: {
	            color: '#CCC'
	         },
	         states: {
	            hover: {
	               fill: '#707073',
	               stroke: '#000000',
	               style: {
	                  color: 'white'
	               }
	            },
	            select: {
	               fill: '#000003',
	               stroke: '#000000',
	               style: {
	                  color: 'white'
	               }
	            }
	         }
	      },
	      inputBoxBorderColor: '#505053',
	      inputStyle: {
	         backgroundColor: '#333',
	         color: 'silver'
	      },
	      labelStyle: {
	         color: 'silver'
	      }
	   },

	   navigator: {
	      handles: {
	         backgroundColor: '#666',
	         borderColor: '#AAA'
	      },
	      outlineColor: '#CCC',
	      maskFill: 'rgba(255,255,255,0.1)',
	      series: {
	         color: '#7798BF',
	         lineColor: '#A6C7ED'
	      },
	      xAxis: {
	         gridLineColor: '#505053'
	      }
	   },

	   scrollbar: {
	      barBackgroundColor: '#808083',
	      barBorderColor: '#808083',
	      buttonArrowColor: '#CCC',
	      buttonBackgroundColor: '#606063',
	      buttonBorderColor: '#606063',
	      rifleColor: '#FFF',
	      trackBackgroundColor: '#404043',
	      trackBorderColor: '#404043'
	   },

	   // special colors for some of the
	   legendBackgroundColor: 'rgba(0, 0, 0, 0.5)',
	   background2: '#505053',
	   dataLabelsColor: '#B0B0B3',
	   textColor: '#C0C0C0',
	   contrastTextColor: '#F0F0F3',
	   maskColor: 'rgba(255,255,255,0.3)'
		};

		// Apply the theme
		Highcharts.setOptions(Highcharts.theme);

		//////////////////////////////////////////////////////////////////////////////////////////////
		var chart = Highcharts.chart('graph1', {

	    title: {
	        text: 'Total Sales for each year'
	    },

	    subtitle: {
	        text: '2011 - 2014'
	    },

	    xAxis: {
	        categories: [
	        	<?php
					include('./../model/connectdb.php');
			        $sql = "SELECT year(OrderDate) AS Year,SUM(TotalDue) AS [TotalSales] FROM [AdventureWorks2014].[Sales].[SalesOrderHeader]
							Group by year(OrderDate) Order by year(OrderDate)";
			    	$stmt = sqlsrv_query( $conn, $sql );
			    	if( $stmt === false) {
			      		die( print_r( sqlsrv_errors(), true) );
			    	}
	        	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      				echo "'".$row['Year']."',";
    			}
    			sqlsrv_free_stmt( $stmt);
    			?>
	        ]
	    },

	    credits: {
        	enabled: false
   		},

	    series: [{
	        type: 'column',
	        colorByPoint: true,
	        name: 'Total Sales',
	        data: [
	        	<?php
					include('./../model/connectdb.php');
			        $sql = "SELECT year(OrderDate) AS Year,SUM(TotalDue) AS [TotalSales] FROM [AdventureWorks2014].[Sales].[SalesOrderHeader]
							Group by year(OrderDate) Order by year(OrderDate)";
			    	$stmt = sqlsrv_query( $conn, $sql );
			    	if( $stmt === false) {
			      		die( print_r( sqlsrv_errors(), true) );
			    	}
	        	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      				echo $row['TotalSales'].",";
    			}
    			sqlsrv_free_stmt( $stmt);
    			?>
	        ],
	        showInLegend: false
	    }]

		});


		$('#plain').click(function () {
	    	chart.update({
	        chart: {
	            inverted: false,
	            polar: false
	        },
	        subtitle: {
	            text: 'Plain'
	        }
	    });
		});

		$('#inverted').click(function () {
	    chart.update({
	        chart: {
	            inverted: true,
	            polar: false
	        },
	        subtitle: {
	            text: 'Inverted'
	        }
	    });
		});

		$('#polar').click(function () {
	    chart.update({
	        chart: {
	            inverted: false,
	            polar: true
	        },
	        subtitle: {
	            text: 'Polar'
	        }
	    });
		});

		////////////////////////////////////////////////////////////////////////////////////////////////////

		Highcharts.chart('graph2', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Number Of Using each Credit Card Type'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
	    credits: {
        	enabled: false
   		},
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [
	        	<?php
					include('./../model/connectdb.php');
			        $sql = "SELECT CardType,count(CardType) * 100.0 / sum(count(CardType)) over()  AS Percentage FROM Sales.SalesOrderHeader SO INNER JOIN Sales.CreditCard CR ON SO.CreditCardID=CR.CreditCardID Group BY CardType";
			    	$stmt = sqlsrv_query( $conn, $sql );
			    	if( $stmt === false) {
			      		die( print_r( sqlsrv_errors(), true) );
			    	}
	        	while( $row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
      				echo "{name:'".$row['CardType']."',y:".$row['Percentage']."},";
    			}
    			sqlsrv_free_stmt( $stmt);
    			?>
            ]
        }]
    });

    ////////////////////////////////////////////////////////////////////////////////////////////////////////
		Highcharts.chart('graph3', {
	    chart: {
	        type: 'column'
	    },
	    title: {
	        text: 'Total sales of each subcategory for each year'
	    },
	    subtitle: {
	        text: '2011-2014'
	    },
	    xAxis: {
	        categories: [
	            '2011',
	            '2012',
	            '2013',
	            '2014'
	        ],
	        crosshair: true
	    },
	    yAxis: {
	        min: 0,
	        title: {
	            text: 'Sales'
	        }
	    },
	    tooltip: {
	        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
	        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
	            '<td style="padding:0"><b>{point.y}</b></td></tr>',
	        footerFormat: '</table>',
	        shared: true,
	        useHTML: true
	    },
	    plotOptions: {
	        column: {
	            pointPadding: 0.2,
	            borderWidth: 0
	        }
	    },
	    credits: {
        	enabled: false
   		},
	    series: [
				<?php
				include('./../model/connectdb.php');
				$catesql ="SELECT Name FROM [Production].[ProductSubcategory] ORDER BY Name";
				$stmtout = sqlsrv_query( $conn, $catesql );
				if( $stmtout === false) {
		      		die( print_r( sqlsrv_errors(), true) );
		    	}
				while( $row = sqlsrv_fetch_array( $stmtout, SQLSRV_FETCH_ASSOC) ) {
					echo "{name:'".$row['Name']."',data:[";
					$datesql ="SELECT year(OrderDate) AS yeara FROM [Sales].[SalesOrderHeader] GROUP BY year(OrderDate) ORDER BY year(OrderDate)";
					$stmt2 = sqlsrv_query( $conn, $datesql );
					if( $stmt2 === false) {
			      		die( print_r( sqlsrv_errors(), true) );
			    	}
			    	while( $row3 = sqlsrv_fetch_array( $stmt2, SQLSRV_FETCH_ASSOC) ) {
							$sql = "SELECT MG.Name, SUM(MG.OrderQty) AS TotalQty, year(SOH.OrderDate) AS years
									FROM [Sales].[SalesOrderHeader] AS SOH
									JOIN (	SELECT PS.ProductID, PS.Name, SOD.OrderQty, SOD.SalesOrderDetailID, SOD.SalesOrderID
											FROM [Sales].[SalesOrderDetail] AS SOD
											JOIN (	SELECT P.ProductID, PSC.Name
													FROM [Production].[Product] AS P
													JOIN [Production].[ProductSubcategory] AS PSC
													ON P.ProductSubcategoryID = PSC.ProductSubcategoryID) AS PS
											ON SOD.ProductID = PS.ProductID) AS MG
									ON SOH.SalesOrderID = MG.SalesOrderID
									Where MG.Name='".$row['Name']."'and year(SOH.OrderDate)='".$row3['yeara']."'
									GROUP BY year(SOH.OrderDate), MG.Name
									ORDER BY MG.Name, years";
					    	$stmt = sqlsrv_query( $conn, $sql );
					    	if( $stmt === false) {
					      		die( print_r( sqlsrv_errors(), true) );
					    	}
					    	while( $row2 = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC) ) {
					    		if ($row2['TotalQty']==null) {
					    			echo "0,";
					    		}
					    		else{
					    			echo $row2['TotalQty'].",";
					    		}
			    			}
			    			sqlsrv_free_stmt( $stmt);
						}

	    			echo "]},";
    			}
    			sqlsrv_free_stmt( $stmtout);
    			?>
				]
			});

	</script>

</div>
