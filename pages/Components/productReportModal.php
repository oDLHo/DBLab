<div class="container-fluid">
  <div class="container">
    <h2 class="text-center">PRODUCTS</h2>
    <p class="text-center">This is part of our products</p>
    <div class="row">
      <div class="col-md-9">
        <div class="panel panel-default">
          <div class="panel-body">
            <br>
            <h3 style="color: red">Out of Stock Products</h3>
            <ul class="list-group">
              <?php
                getOutOfStock();
              ?>
            </ul>
            <br>
          </div>
        </div>
      </div>

      <div class="col-md-3">
        <div class="panel panel-default">
          <div class="panel-body">
            <?php
              getNumberOfOutOfStock();
            ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
