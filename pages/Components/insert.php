<?php
        $location = $_POST['location'];
        $product = $_POST['product'];
        $shelf = $_POST['shelf'];
        $bin = $_POST['bin'];
        $quantity = $_POST['quantity'];
        
        $alertStmt = "";

        include('../../model/connectdb.php');   
        if($location==null|| $product==null||$shelf==null||$bin==null||$quantity==null||$location=='all'||$product=='all'){
            $alertStmt = $alertStmt."\\n-Please select Product, Location and fill out this form.";
        }else{
            $sqlToCheck = "SELECT * FROM Production.ProductInventory WHERE ProductID = ".$product." AND LocationID = ".$location;
            $stmt = sqlsrv_query( $conn, $sqlToCheck, array(), array("Scrollable"=>"buffered") );
            $row_count = sqlsrv_num_rows( $stmt ); 
            if($row_count>0){
                $alertStmt = $alertStmt."\\n-There are already Product in This Location.";
            }else{
                $alertStmt = $alertStmt."\\n-Shelf should be a-z, A-Z or N/A.";
                $alertStmt = $alertStmt."\\n-Bin should be number between 0-100.";
                $alertStmt = $alertStmt."\\n-Quantity should be number between 0-32,767.";
//                if($shlef!="N/A"){
//                    if( !preg_match( '~[A-Z]~', $shelf) && !preg_match( '~[a-z]~', $shelf) && (strlen( $shelf) > 1)){
//                        $alertStmt = $alertStmt."\\n-Shelf should be a-z, A-Z or N/A.";
//                    }
//                }
//                if(is_int($bin)&&$bin>=0&&$bin<=255){
//                   
//                }
//                else{
//                    $alertStmt = $alertStmt."\\-Bin should be number between 0-100.";
//                }
//                if(!is_int($quantity)||$quantity<0||$quantity>32767){
//                    $alertStmt = $alertStmt."\\-Quantity should be number between 0-32,767.";
//                }
            }

        }
           
            
        $sql = "DECLARE @rowguid uniqueidentifier = NewID();
                INSERT INTO [Production].[ProductInventory] (ProductID,LocationID,Shelf,Bin,Quantity,rowguid,ModifiedDate)
                VALUES ('".$product."','".$location."','".$shelf."',".$bin.",".$quantity.",@rowguid,GETDATE())";
        $stmt = sqlsrv_query( $conn, $sql );
        if( $stmt === false) {  
            echo "<script>alert('Insert Incomplete.".$alertStmt."');</script>"; 
            echo "<script>console.log( print_r( sqlsrv_errors(), true) );</script>";
            echo "<script>window.location = '../admin.php'</script>";
        }
        else {
            echo "<script>alert('Insert Complete');</script>";
            echo "<script>window.location = '../admin.php'</script>";
        }
        
        sqlsrv_free_stmt( $stmt);
?>