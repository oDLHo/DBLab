<div id="contentModal" class="modalBody modal-animation">
  <div class="container">
    <div class="row" style="margin:0px, auto, 0px, auto;">
      <div class="col-md-12" style="margin-top: 40px; margin-bottom: 20px;">
          Location :
          <select class="tableContentSelecter form-control" id="tableContentSelecter" onchange="showContentRecord()">
            <?php selectLocation(); ?>
          </select>
      </div>
    </div>
    <div class="row">
      <div class="contentHandle" id="recordHandle">
        <!-- show all record from table -->
        <?php showRecord("all"); ?>
      </div>
    </div>
  </div>
</div