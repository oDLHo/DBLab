<div id="insertModal" class="modalBody modal-animation">
    <form action="./Components/insert.php" method="post" name="formInsertSubmit" id="formInsertSubmit">
        <div class="row form-group">
            <div class="col-md-12">
                <label for="location">Location:</label>
                <select class="form-control" id="location" name="location">
                       <?php selectLocation(); ?>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12">
                <label for="product">Product:</label>
                <select class="form-control" id="product" name="product">
                       <?php selectProduct(); ?>
                </select>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-10">
                <label for="shelf">Shelf:</label>
                <input type="text" class="form-control" id="shelf" name="shelf" placeholder="ex. A">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-10">
                <label for="bin">Bin:</label>
                <input type="number" class="form-control" id="bin" name="bin" placeholder="ex. 9">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-sm-10">
                <label for="quantity">Quantity:</label>
                <input type="number" class="form-control" id="quantity" name="quantity" placeholder="ex. 99">
            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-default">Insert</button>
    </form>
    
    
    <div id="alertInsert">

    </div>
    
</div>

