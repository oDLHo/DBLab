<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Bootstrap style link -->
    <link href="./../assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="./../assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- Custom styles link -->
    <link rel="stylesheet" href="./../assets/css/ceo.css">
    <!-- Link JQuery -->
    <script type="text/javascript" src="./../assets/js/jquery.js"></script>
    <!-- Link Modal function -->
    <script type="text/javascript" src="./../controllers/modalController.js"></script>
    <!-- Link Ajax function -->
    <script type="text/javascript" src="./../controllers/ajaxController.js"></script>

    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-more.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>

    <title>CEO : Adventure Work Company</title>
  </head>
  <body style="background-image: url('./../assets/Image/maxresdefault.jpg');width:96%;">
    <?php include('./../controllers/ceoController.php'); ?>
      <div class="button-bar">
          <!-- call function to show the selected modal when click -->
          <button class="ghost-button" onclick="showSaleReport()">Show Sale Report</button>
          <button class="ghost-button" onclick="showHRReport()">Show Human Resources Report</button>
          <button class="ghost-button" onclick="showProductReport()">Show Product Report</button>
      </div>

      <div class="formBox">
        <div class="form-bodies isShowing animate-in" id="saleHandle">
          <!-- Include insertModal -->
          <?php require('./Components/saleReportModal.php'); ?>
        </div>

        <div class="form-bodies" id="hrHandle">
          <!-- Include insertModal -->
          <?php require('./Components/hrReportModal.php'); ?>
        </div>

        <div class="form-bodies" id="productHandle">
          <!-- Include contentModal -->
          <?php require('./Components/productReportModal.php'); ?>
        </div>
      </div>



  </body>
</html>
