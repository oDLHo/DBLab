<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--Bootstrap style link -->
    <link href="./../assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="./../assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <!-- Custom styles link -->
    <link rel="stylesheet" href="./../assets/css/admin.css">
    <!-- Link JQuery -->
    <script type="text/javascript" src="./../assets/js/jquery.js"></script>
    <!-- Link Modal function -->
    <script type="text/javascript" src="./../controllers/modalController.js"></script>
    <!-- Link Ajax function -->
    <script type="text/javascript" src="./../controllers/ajaxController.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!--     <script type="text/javascript">
      
          function showEditto(reportNum){
      console.log(reportNum);
    }
    </script> -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    
    <title>Admin : Adventure Work Company</title>
  </head>
  <body style="background-image: url('./../assets/Image/maxresdefault.jpg');width:96%;">
    <?php include('./../controllers/adminController.php'); ?>
      <div class="button-bar">
          <!-- call function to show the selected modal when click -->
          <button class="ghost-button" onclick="showContent()">Show Record</button>
          <button class="ghost-button" onclick="showInsert()">Insert Record</button>
      </div>

      <div class="formBox">
        <div class="form-bodies isShowing animate-in" id="insertHandle">
          <!-- Include insertModal -->
          <?php require('./Components/insertModal.php'); ?>
        </div>
        <div class="form-bodies" id="contentHandle">
          <!-- Include contentModal -->
          <?php require('./Components/contentModal.php'); ?>
        </div>
      </div>
  </body>
</html>
